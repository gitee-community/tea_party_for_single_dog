上周五我们碰瓷情人节，来了场专属单身汪的茶话会（[活动入口](https://gitee.com/Selected-Activities/tea_party_for_single_dog)） 
![输入图片说明](https://images.gitee.com/uploads/images/2020/0217/163256_e1d9c978_1899542.jpeg "微信公众号.jpg")    
  
这次茶话会，单身汪们都脑洞大开，出现了很多很好玩的评论。但由于奖品有限，小编只能万般苦恼地从每个话题中选出了五位单身汪送出我们的：Gitee 官方T恤 或 Gitee 巨型鼠标垫  
![输入图片说明](https://images.gitee.com/uploads/images/2020/0217/163325_cdd74070_1899542.png "奖品.png")  

注：小编已经私信联系了中奖的单身汪们，大家记得康康自己的私信，及时回复哟～  

---------------------  

#### #相亲区：听到请回答，我那下落不明的爱情# 
 
**奖品：** Gitee 巨型鼠标垫

 **中奖用户：** @Hayley，@obhen，@ArianaGrande13，@今天天气真好QwQ，@四色鬼

 **获奖评论展示**   

@Hayley ：  
![输入图片说明](https://images.gitee.com/uploads/images/2020/0217/163453_92703108_1899542.png "相亲-heyley.png")  
  
@obhen：  
![输入图片说明](https://images.gitee.com/uploads/images/2020/0217/163512_b87d1afb_1899542.png "相亲-obhen.png")  
  
@ArianaGrande13  
![输入图片说明](https://images.gitee.com/uploads/images/2020/0217/163532_7a91ed5c_1899542.png "相亲-ar.png")  
  
@今天天气真好QwQ  
![输入图片说明](https://images.gitee.com/uploads/images/2020/0217/163553_c431624e_1899542.png "相亲-今天天气真好.png")  
  
@四色鬼  
![输入图片说明](https://images.gitee.com/uploads/images/2020/0217/163612_f3be9518_1899542.png "相亲-四色鬼.png")   
   

虽然活动已经结束了，但是寻找爱情的道路还在继续着～如果你还在努力寻找自己下落不明的爱情，可以戳这里继续参与：[传送门](https://gitee.com/Selected-Activities/tea_party_for_single_dog/issues/I19CP1)

----------------------

#### #单身汪表情包#

 **奖品：** Gitee 巨型鼠标垫  

 **中奖用户：** @lay， @落落，@achin，@AmazingPromise，@chaoZhang0613  

 **获奖展示**   

@lay   
![输入图片说明](https://images.gitee.com/uploads/images/2020/0217/163733_6f13e1af_1899542.png "表情包-lay.png")   
    
@落落  
![输入图片说明](https://images.gitee.com/uploads/images/2020/0217/163751_c747e1c2_1899542.png "表情包-落落.png")  
  
@achin  
![输入图片说明](https://images.gitee.com/uploads/images/2020/0217/163806_6bb16a9c_1899542.png "表情包-achin.png")  
    
@AmazingPromise  
![输入图片说明](https://images.gitee.com/uploads/images/2020/0217/163844_c9cd3905_1899542.png "表情包-amp.png")  
  
@chaoZhang0613  
![输入图片说明](https://images.gitee.com/uploads/images/2020/0217/163901_b0028e1d_1899542.png "表情包-chaozhang.png")  
    
收更多快乐单身汪表情包请戳：[传送门](https://gitee.com/Selected-Activities/tea_party_for_single_dog/issues/I19CIW)
 

--------------------

#### #单身秘籍#

 **奖品：** Gitee 巨型鼠标垫    

 **中奖用户** ：@果果，@异步加载，@青草，@River ，@binperson  

 **获奖展示：**   

@果果    
![输入图片说明](https://images.gitee.com/uploads/images/2020/0217/164033_9c3f74dd_1899542.png "单身秘籍-果果.png")  
  
@异步加载  
![输入图片说明](https://images.gitee.com/uploads/images/2020/0217/164056_e77a4267_1899542.png "单身秘籍-异步加载.png")  
    
@青草  
![输入图片说明](https://images.gitee.com/uploads/images/2020/0217/164113_43d6dc31_1899542.png "单身秘籍-青草.png")  
  
@River  
![输入图片说明](https://images.gitee.com/uploads/images/2020/0217/164130_826837cc_1899542.png "单身秘籍-river.png")  
    
@binperson  
![输入图片说明](https://images.gitee.com/uploads/images/2020/0217/164151_9241a2c7_1899542.png "单身秘籍-bin.png")    
  
获得更多单身秘籍请戳：[传送门](https://gitee.com/Selected-Activities/tea_party_for_single_dog/issues/I19CP4)       
    
---------------------  
  
#### #一个人如何优雅度过情人节#

 **奖品：** Gitee 官方T恤  

 **中奖用户：** @无畏地主，@路顺，@凡人，@hazlank ，@花间小酌   
  
 **获奖展示：**   

@无畏地主  
![输入图片说明](https://images.gitee.com/uploads/images/2020/0217/164251_044db9f4_1899542.png "一个人-无畏地主.png")  
   
@路顺  
![输入图片说明](https://images.gitee.com/uploads/images/2020/0217/164313_2f877340_1899542.png "一个人-路顺.png")  
    
@凡人  
![输入图片说明](https://images.gitee.com/uploads/images/2020/0217/164330_8617e694_1899542.png "一个人-凡人.png")     
  
@hazlank   
![输入图片说明](https://images.gitee.com/uploads/images/2020/0217/164353_4d7414a9_1899542.png "一个人-hazlank.png")  
    
@花间小酌  
![输入图片说明](https://images.gitee.com/uploads/images/2020/0217/164415_1cf3ce9c_1899542.png "一个人-花间小酌.png")  
    
get 更多一个人过情人节的方式请戳：[传送门](https://gitee.com/Selected-Activities/tea_party_for_single_dog/issues/I19CKW)  
  
-----------------------------------  
  
#### #理想的另一半#

 **奖品：** Gitee 官方T恤  

 **中奖用户：** @一生何求，@卫志强，@宇润，@dido_imaflt ，@摘星星种星星  

 **获奖展示：**   
  
@一生何求  
![输入图片说明](https://images.gitee.com/uploads/images/2020/0217/164519_40bd36c1_1899542.png "理想-一生何求.png")  
  
@卫志强  
![输入图片说明](https://images.gitee.com/uploads/images/2020/0217/164534_0312b6ea_1899542.png "理想-卫.png")  
  
@宇润  
![输入图片说明](https://images.gitee.com/uploads/images/2020/0217/164550_6776f36b_1899542.png "理想-宇润.png")  
  
@摘星星种星星  
![输入图片说明](https://images.gitee.com/uploads/images/2020/0217/164605_950dd619_1899542.png "理想-摘星星.png")  
   
@dido_imaflt     
![输入图片说明](https://images.gitee.com/uploads/images/2020/0217/164626_0d658e9d_1899542.png "理想-dido.png")  
    
你理想的另一半又是谁呢？分享请戳：[传送门](https://gitee.com/Selected-Activities/tea_party_for_single_dog/issues/I19CKO)   
  
感谢大家对本次活动的支持！欢迎继续关注 [Gitee 星球的小活动](https://gitee.com/Selected-Activities)，我们将会不时为大家推出一些有意思的活动，欢迎常来玩～