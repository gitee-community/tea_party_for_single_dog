# 实力碰瓷情人节——单身汪茶话会

情人节，一个令单身汪一言难尽的节日……
  
往年的 2.14 吧，满大街的情侣，出门吃个饭都要被迫吃一顿狗粮，分分钟遭到一万点伤害 
          
![输入图片说明](https://images.gitee.com/uploads/images/2020/0212/112307_e962be97_1899542.png "200-听，狗哭的声音.png")        
    
但是！今年，有对象的朋友们，听我一句劝，老实在家待着，不要去（xiù）约（ēn）会（ài）！       
     
![输入图片说明](https://images.gitee.com/uploads/images/2020/0211/194255_e463a77b_1899542.png "200-不吃，谢谢！.png")    
     
再说，情人节有什么了不起？凭一己之力单身二十几年的我骄傲了吗？  

而且我，文能写代码，武能随时随地写代码     

作为一个身怀技术的大佬，在朋友圈/微博晒照片这种操作，怎么体现我的身份？

必须用仓库啊！
    
来，用我们的方式，一起唠唠吧～
 
#### 如何参与？
任意点击以下话题参与评论，Gitee 的资深单身使者会在活动结束后从 **每个**话题中选出 5 个精彩分享，送上“情人节碰瓷大礼包”！  

- [#凭实力单身秘籍#](https://gitee.com/gitee-community/tea_party_for_single_dog/issues/I19CP4)  
- [#单身狗表情包#](https://gitee.com/gitee-community/tea_party_for_single_dog/issues/I19CIW)           
- [#一个人如何优雅度过情人节#](https://gitee.com/gitee-community/tea_party_for_single_dog/issues/I19CKW)                       
- [#理想的另一半#](https://gitee.com/gitee-community/tea_party_for_single_dog/issues/I19CKO)    
- [#相亲区：听到请回答，我那下落不明的爱情#](https://gitee.com/gitee-community/tea_party_for_single_dog/issues/I19CP1)      
  
#### 有奖活动截止时间

2020年2月14日23：59

#### 奖品
情人节碰瓷大礼包：  
10 件价值 99元 的 [Gitee 官方T恤](https://item.taobao.com/item.htm?spm=a1z10.1-c-s.w4004-21733633067.5.5b47194dWJCxVU&id=597155407543)（点击查看奖品信息）  
![输入图片说明](https://images.gitee.com/uploads/images/2020/0214/071142_a3238f7f_1899542.png "礼包1.png")    
15 个价值 89元 的 [Gitee 巨型鼠标垫](https://item.taobao.com/item.htm?spm=a1z10.1-c-s.w4001-21733612070.3.5b47194dWJCxVU&id=597157043586&scene=taobao_shop)（点击查看奖品信息）   
![输入图片说明](https://images.gitee.com/uploads/images/2020/0214/071200_f1493732_1899542.png "礼包-2.png")    
    
 **PS：获奖名单将在 2 月 16 日公布，并私信联系获奖的大佬，请大家继续关注嘿～（因为疫情的原因，奖品可能会迟到……但从永远不会缺席！）**  

### 写在最后的话

本次活动，希望能让大家在因为疫情神经紧绷压抑的状态下，放松下来好好的玩一玩、笑一笑，笑过以后，我们依然保持虔诚的心为祖国加油！愿阴霾过后，春暖花开～


--------------------
### 第一届单身汪茶话会中奖名单公布

|  话题   |  中奖用户   |
| --- | --- |
|  [#凭实力单身秘籍#](https://gitee.com/gitee-community/tea_party_for_single_dog/issues/I19CP4)    | @果果，@异步加载，@青草，@River ，@binperson    |
|  [#单身狗表情包#](https://gitee.com/gitee-community/tea_party_for_single_dog/issues/I19CIW)     |  @lay， @落落，@achin，@AmazingPromise，@chaoZhang0613   |
|   [#一个人如何优雅度过情人节#](https://gitee.com/gitee-community/tea_party_for_single_dog/issues/I19CKW)     | @无畏地主，@路顺，@凡人，@hazlank ，@花间小酌    |
|  [#理想的另一半#](https://gitee.com/gitee-community/tea_party_for_single_dog/issues/I19CKO)     |  @一生何求，@卫志强，@宇润，@dido_imaflt ，@摘星星种星星   |
| [#相亲区：听到请回答，我那下落不明的爱情#](https://gitee.com/gitee-community/tea_party_for_single_dog/issues/I19CP1) |  @Hayley，@obhen，@ArianaGrande13，@今天天气真好QwQ，@四色鬼 |